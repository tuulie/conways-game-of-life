#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/* compile and run from a Unix-based terminal: ./filename iterations y x ...

int l in main() should correspond to length of arr[][] and arr2[][]

try this:

./filename 100 6 8 6 6 8 7 8 9 8 10 10 12 10 9 */

static int arr[32][32] = {0};

static int arr2[32][32] = {0};

void drawTable(int l) {

	fputs("\n\t\t      Conway's Game of Life\n", stdout);
	
	int l2 = l + 1;	

	for (int topt = 0; topt < l2; topt++) {
		fputs(" v", stdout);
	}	
	for (int y = 0; y < l; y++) {

		fputs("\n>", stdout);

		for (int x = 0; x < l; x++) {

			int n = arr[y][x];

			if (n == 0) {
				fputs("  ", stdout);
			}
			else {
				fputs("()", stdout);
			}
		}
		fputs(" <", stdout);
	}
	fputs("\n", stdout);
	
	for (int btmt = 0; btmt < l2; btmt++) {
		fputs(" ^", stdout);
	}
	fputs("\n", stdout);
}

int countNeighbors(int l, int y, int x) {

	int n = 0;

	signed int ycoords[8] = {y-1, y, y+1, y+1, y+1, y, y-1, y-1};
	
	signed int xcoords[8] = {x-1, x-1, x-1, x, x+1, x+1, x+1, x};
	
	for (int i = 0; i < 8; i++) {
		int y0 = ycoords[i];
		int x0 = xcoords[i];
		if (y0 > 0 && x0 > 0 &&
		    y0 < l && x0 < l && arr[y0][x0] == 1) {
			n++;
		}
		else {
			continue;
		}
	}
	return n;
}

void calcTable(int l) {

	for (int y = 0; y < l; y++)
		
		for (int x = 0; x < l; x++) {
			
			int n = countNeighbors(l, y, x);

			if (arr[y][x] == 0 && n == 3) {
				arr2[y][x] = 1;
			}
			else if (arr[y][x] == 1 && n == 2) {
				arr2[y][x] = 1;
			}
			else if (arr[y][x] == 1 && n == 3) {
				arr2[y][x] = 1;
			}
			else {
				arr2[y][x] = 0;
			}
		}
}

void runTicks(int t, int l) {
	
	int count = 0;

	while (count < t) {
		drawTable(l);
		calcTable(l);

		for (int j = 0; j < l; j++) {
			for (int k = 0; k < l; k++) {
				arr[j][k] = arr2[j][k];
			}
		}
		count++;
		usleep(300000);
	}
}

int main(int argc, char *argv[]) {
	
	int l = 32;

	int limit = l * l + 2;

	if (argc % 2 != 0 || argc < 8 || argc > limit) {
		fputs("Quantity of coordinates must be even and between 6 and 1024\n", stdout);
		return 0;
	}

	for (int i = 2; i < argc; i += 2) {
		int n = i + 1;
		signed int y = atoi(argv[i]);
		signed int x = atoi(argv[n]);

		if (y > 0 && y < l && x > 0 && x < l) {
			arr[y][x] = 1;
		}
		else {
			fputs("Value of coordinates must be between 0 and 31\n", stdout);
			return 0;
		}
	}

	int t = atoi(argv[1]);

	if (t < 0 || t > 2000) {
		fputs("Ticks should be between 0 and 2000\n", stdout);
		return 0;
	}

	runTicks(t, l);

	return 0;
}

